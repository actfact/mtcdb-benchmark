/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.database;

import java.sql.Connection;
import java.sql.SQLException;

import com.actfact.mtcb.model.Trx;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class TrxImpl implements Trx
{

	private Connection conn;
	private String name;

	public static TrxImpl get(Connection conn, String name)
	{
		return new TrxImpl(conn, name);
	}

	private TrxImpl(Connection conn, String name)
	{
		if (conn == null)
			throw new IllegalStateException("Must provide a connection to instantiate a transaction");
		this.conn = conn;
		this.name = name;
	}

	public Connection getConnection()
	{
		if (conn == null)
			throw new IllegalStateException("Transaction was already closed");
		return conn;
	}

	public String getName()
	{
		return name;
	}

	public void commit()
	{
		try
		{
			conn.commit();
		}
		catch (SQLException e)
		{
			throw new UncheckedSQLException(e);
		}
	}

	public void rollback()
	{
		try
		{
			conn.rollback();
		}
		catch (SQLException e)
		{
			throw new UncheckedSQLException(e);
		}
	}

}
