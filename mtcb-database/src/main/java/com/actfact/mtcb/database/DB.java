/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import com.actfact.mtcb.main.Timing;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Trx;

/**
 * 
 * 
 * @author Wim van der Zijden
 * @author ActFact Projets B.V.
 * (c) 2017
 */
public class DB
{

	public enum Driver
	{
		POSTGRESQL, ORACLE, SQLSERVER
	}

	/**
	 * Make sure to only call this method once per thread.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection(Driver driver, String host, String name, String user, String password) throws SQLException
	{
		try
		{
			long before = System.nanoTime();
			switch (driver)
			{
			case POSTGRESQL:
				Class.forName("org.postgresql.Driver");
				Connection connection = null;
				// connection =
				// DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/ingrid");
				connection = DriverManager.getConnection(
						"jdbc:postgresql://" + host + ":5432/" + name, user,
						password);
				connection.setAutoCommit(false);
				Timing.register("ESTABLISH DB CONNECTION", System.nanoTime() - before);
				return connection;
			default:
				throw new UncheckedSQLException("Not implemented: " + driver);
			}
		}
		catch (ClassNotFoundException e)
		{
			System.err.println("Driver not found");
			e.printStackTrace();
			throw new RuntimeException("Driver not found");
		}
	}

	public static void closeStatement(Statement st)
	{
		try
		{
			if (st != null)
				st.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void closeResultSet(ResultSet rs)
	{
		try
		{
			if (rs != null)
				rs.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Execute a SQL statement that does not return a result set.
	 * 
	 * @param sql
	 * @throws SQLException
	 */
	public static void executeUpdate(Trx trx, String sql, Object... params)
	{
		PreparedStatement pstmt = null;
		try
		{
			pstmt = ((TrxImpl) trx).getConnection().prepareStatement(sql);
			int i = 0;
			for (Object p : params)
			{
				if (p instanceof PO)
					pstmt.setObject(++i, ((PO) p).getID());
				else
					pstmt.setObject(++i, p);
				
			}
			pstmt.execute();
			// System.out.println("SQL=" + sql);
		}
		catch (SQLException e)
		{
			System.err.println("SQL=" + sql + Arrays.asList(params));
			System.err.println(e.getMessage());
			throw new UncheckedSQLException(e);
		}
		finally
		{
			closeStatement(pstmt);
		}
	}

	public static void executeUpdateOkToFail(Trx trx, String sql, Object... params)
	{
		try
		{
			executeUpdate(trx, sql, params);
		}
		catch (UncheckedSQLException e)
		{
			// Error is already logged in the called method
		}
	}

	public static PreparedStatement prepareStatement(Trx trx, String sql) throws SQLException
	{
		return ((TrxImpl) trx).getConnection().prepareStatement(sql);
	}
	
}
