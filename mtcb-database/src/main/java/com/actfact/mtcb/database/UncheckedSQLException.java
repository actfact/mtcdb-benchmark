/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.database;

/**
 * An unchecked SQL exception
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 *
 */
public class UncheckedSQLException extends RuntimeException
{

	private static final long serialVersionUID = 1L;

	public UncheckedSQLException(String message)
	{
		super(message);
	}
	
	public UncheckedSQLException(Exception e)
	{
		super(e);
	}
}
