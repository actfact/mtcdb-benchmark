# MTCB: A Multi-Tenant Customizable database Benchmark #

This is the implementation of a Multi-Tenant Customizable Database (MTC-DB) Benchmark that was developed by Wim van der Zijden in cooperation with ActFact Projects B.V. and the University of Twente.

## Getting Started (Linux) ##

The repository is comprised of three Eclipse Projects. One way of running the code on your local machine is using Eclipse to import the projects "As existing projects" into your workspace. The other way is using the command line on a linux machine. In this guide we will only cover the latter method.

### Prerequisites ###
The following applications need to be installed on your system:

* [Java SE SDK version 8+](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

* [Git](https://www.atlassian.com/git/tutorials/install-git/linux)

* [Apache Maven](http://maven.apache.org/install.html)

* [PostgreSQL 9.6+](https://www.postgresql.org/download/) (unless you are using a database server)

### Setup and run ###
Navigate to your git folder and clone the repository:

```git clone https://actfact@bitbucket.org/actfact/mtcdb-benchmark.git```

Make the bash scripts executable and run the maven installer:

```
cd mtcdb-benchmark
chmod u+x *.sh
./mvn-install.sh
```

Make sure this prints "BUILD SUCCESS" three times.

Now run the benchmark with the command below.

For profile, use tiny, small, medium or all.

For dbHost, you can usually use localhost. PostgreSQL must run on the default port 5432.

For dbName provide the name of the database. Be careful, the code will drop all existing tables and schemas in that database.

For dbUser and dbPass provide a Postgres username and password.

```
./run.sh <profile> <dbHost> <dbName> <dbUser> <dbPass>
```

Example command:
```
./run.sh tiny localhost mtcdb_schemabased postgres postgres
```

The output of the benchmark will be printed to the console as well as written to a log file in the current folder.

### Feedback ###

Wim van der Zijden <w.vanderzijden@actfact.com>

ActFact Projects www.actfact.com