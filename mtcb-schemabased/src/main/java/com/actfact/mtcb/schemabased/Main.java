/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.schemabased;

import com.actfact.mtcb.database.DB;
import com.actfact.mtcb.exception.MTCBException;
import com.actfact.mtcb.main.Benchmark;
import com.actfact.mtcb.main.Profile;
import com.actfact.mtcb.model.MTCDB;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class Main
{

	public static void main(String[] args)
	{
		if (args.length != 5)
		{
			System.err.println("Must provide exactly 5 arguments: profile dbHost dbName dbUser dbPass. Example: tiny localhost schemabased postgres postgres");
			return;
		}
		
		Profile[] profiles = getProfile(args[0]);
		String host = args[1];
		String name = args[2];
		String user = args[3];
		String pass = args[4];
		int repeats = 5;
		
		for (Profile profile : profiles)
		{
			System.out.println("***** Profile: " + profile + " *****");
			MTCDB mtcdb = new MTCDB_SchemaBased(DB.Driver.POSTGRESQL, host, name, user, pass);
			Benchmark.aulbachTest(mtcdb);
			Benchmark.setup(profile, mtcdb);
			for (int n = 0; n < repeats; n++)
				Benchmark.main(profile, mtcdb);
		}
	}
	
	private static Profile[] getProfile(String name)
	{
		if (name.toUpperCase().equals("ALL"))
			return Profile.values();
		for (Profile profile : Profile.values())
			if (profile.name().equals(name.toUpperCase()))
				return new Profile[] { profile };
		throw new MTCBException("Unknown profile: " + name + ". Options are tiny, small, medium and all");
	}

}
