/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.schemabased;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.actfact.mtcb.database.DB;
import com.actfact.mtcb.database.UncheckedSQLException;
import com.actfact.mtcb.main.Timing;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.system.SystemType;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class AttributeImpl extends POImpl implements Attribute
{
	
	public AttributeImpl(Ctx ctx, UUID id, Trx trx)
	{
		super(ctx, SystemType.ATTRIBUTE, id, trx);
	}

	public AttributeImpl(Ctx ctx, ResultSet rs, Trx trx) throws SQLException
	{
		super(ctx, SystemType.ATTRIBUTE, rs, trx);
	}

	public AttributeImpl(Ctx ctx, Trx trx)
	{
		super(ctx, SystemType.ATTRIBUTE, trx);
	}

	@Override
	public boolean save()
	{
		long started = System.nanoTime();
		boolean isNew = isNew();
		if (!super.save())
			return false;
		// Value of isNew must be preserved
		boolean result = doDDL(getContext(), isNew, getTrx());
		String operationName = isNew ? "CREATE" : "UPATE";
		Timing.register(operationName + " Attribute", System.nanoTime() - started);
		return result;
	}
	
	private boolean doDDL(Ctx ctx, boolean isNew, Trx trx)
	{
		if (isNew)
		{
			String sql = "ALTER TABLE " + POImpl.getTableName(ctx, getMasterType(), false) +
				" ADD COLUMN " + getName() + " " + TypeImpl.getSQLDataType(getDataType());
			//System.out.println(sql);
			DB.executeUpdate(trx, sql);
			sql = "CREATE INDEX " + getMasterType().getName() + "_" + getName() + " ON " + POImpl.getTableName(ctx, getMasterType(), false) +
					" (" + getName() + ")";
			DB.executeUpdate(trx, sql);
			return true;
		}
		throw new UncheckedSQLException("Updatable fields not yet implemented");
	}

}
