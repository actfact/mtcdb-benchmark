/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.schemabased;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.actfact.mtcb.database.DB;
import com.actfact.mtcb.database.UncheckedSQLException;
import com.actfact.mtcb.exception.NoSuchAttributeException;
import com.actfact.mtcb.main.Timing;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.system.SystemType;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class TypeImpl extends POImpl implements Type
{

	private volatile List<Attribute> attributes;
	private Map<String, Attribute> nameToAttr;
	private Map<UUID, Attribute> idToAttr;

	public TypeImpl(Ctx ctx, ResultSet rs, Trx trx) throws SQLException
	{
		super(ctx, SystemType.TYPE, rs, trx);
	}

	public TypeImpl(Ctx ctx, UUID id, Trx trx)
	{
		super(ctx, SystemType.TYPE, id, trx);
	}

	public TypeImpl(Ctx ctx, Trx trx)
	{
		super(ctx, SystemType.TYPE, trx);
		this.attributes = new ArrayList<>();
		this.nameToAttr = new HashMap<>();
		this.idToAttr = new HashMap<>();
	}

	public List<Attribute> getAttributes()
	{
		List<Attribute> attributes = this.attributes;
		if (attributes == null)
		{
			synchronized (this)
			{
				attributes = this.attributes;
				if (attributes == null)
				{
					long started = System.nanoTime();
					this.attributes = attributes = new ArrayList<>();
					nameToAttr = new HashMap<>();
					idToAttr = new HashMap<>();
					String sql = "SELECT * "
							+ "FROM " + getTableName(getContext(), SystemType.ATTRIBUTE, true) + " t"
							+ " WHERE MasterType = ?";
					//System.out.println(sql);
					try (PreparedStatement pstmt = DB.prepareStatement(getTrx(), sql))
					{
						pstmt.setObject(1, getID());
						ResultSet rs = pstmt.executeQuery();
						while (rs.next())
						{
							Attribute attr = new AttributeImpl(getContext(), rs, getTrx());
							attributes.add(attr);
							nameToAttr.put(attr.getName(), attr);
							idToAttr.put(attr.getID(), attr);
						}
						// TODO add global columns (or just implement them the standard way without creating magic specials?)
						//attributes.add(SystemAttribute.GLOBAL_CREATED);
						//nameToAttr.put(SystemAttribute.GLOBAL_CREATED.getName(), SystemAttribute.GLOBAL_CREATED);
						//System.out.println(sql + "[" + id + "] results=" + attributes.size() + " type=" + getName());
					}
					catch (SQLException e)
					{
						throw new UncheckedSQLException(e);
					}
					Timing.register("GET Attributes for Type", System.nanoTime() - started);
				}
			}
		}
		return attributes;
	}

	@Override
	public Attribute getAttribute(String name)
	{
		getAttributes();
		Attribute attr = nameToAttr.get(name);
		if (attr == null)
			throw new NoSuchAttributeException(getName(), name);
		return attr;
	}

	@Override
	public boolean save()
	{
		long started = System.nanoTime();
		boolean isNew = isNew();
		if (!super.save())
			return false;
		// Value of isNew must be preserved
		boolean result = doDDL(getContext(), this, isNew, getTrx());
		String operationName = isNew ? "CREATE" : "UPATE";
		Timing.register(operationName + " Type", System.nanoTime() - started);
		return result;
	}

	public static boolean doDDL(Ctx ctx, Type type, boolean isNew, Trx trx)
	{
		//if (type.getAttributes().size() == 0)
		//	return true;
		if (isNew)
		{
			String sql = "";
			sql = "DROP TABLE IF EXISTS " + POImpl.getTableName(ctx, type, false) + ";";
			DB.executeUpdate(trx, sql);
			sql = "";
			for (Attribute attr : type.getAttributes())
				sql += ", " + attr.getName() + " " + getSQLDataType(attr.getDataType());
			sql = "CREATE TABLE " + POImpl.getTableName(ctx, type, false) +
					" (Tenant UUID, ID UUID PRIMARY KEY" + sql + ")";
			//System.out.println(sql);
			DB.executeUpdate(trx, sql);
			type.getAttributes()
					.stream()
					.filter(a -> a.isIndexed())
					.forEach(attr -> DB.executeUpdate(trx,
							"CREATE INDEX " + type.getName() + "_" + attr.getName() + " ON " + POImpl.getTableName(ctx, type, false) +
									" (" + attr.getName() + ")"));
		}
		else
		{
			throw new UncheckedSQLException("Updateable types not yet implemented");
			// No updateable fields (yet)
		}
		if (type.getAttributes() != null)
			for (Attribute attr : type.getAttributes())
				if (attr.isNew())
					if (!attr.save())
						return false;
		return true;
	}

	public static String getSQLDataType(Type dataType)
	{
		switch (dataType.getName())
		{
		case "String":
			return "TEXT";
		case "Number":
			return "NUMERIC";
		case "Boolean":
			return "BOOLEAN";
		case "Timestamp":
			return "TIMESTAMP";
		default:
			return "UUID";
		}
	}

	@Override
	public void addAttributes(Attribute... attributes)
	{
		this.getAttributes().addAll(Arrays.asList(attributes));
		Arrays.asList(attributes).stream().forEach(a -> nameToAttr.put(a.getName(), a));
	}

	@Override
	public Attribute getAttribute(UUID id)
	{
		getAttributes();
		Attribute attr = idToAttr.get(id);
		if (attr == null)
			throw new NoSuchAttributeException(getName(), id.toString());
		return attr;
	}

}
