/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.schemabased;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.User;
import com.actfact.mtcb.system.SystemType;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class UserImpl extends POImpl implements User
{

	public UserImpl(Ctx ctx, UUID id, Trx trx)
	{
		super(ctx, SystemType.USER, id, trx);
	}

	public UserImpl(Ctx ctx, ResultSet rs, Trx trx) throws SQLException
	{
		super(ctx, SystemType.USER, rs, trx);
	}

	public UserImpl(Ctx ctx, Trx trx)
	{
		super(ctx, SystemType.USER, trx);
	}

}
