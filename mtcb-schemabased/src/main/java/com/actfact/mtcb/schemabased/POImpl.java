/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.schemabased;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.actfact.mtcb.database.DB;
import com.actfact.mtcb.database.UncheckedSQLException;
import com.actfact.mtcb.main.Timing;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.AttributeValue;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.system.SystemPO;
import com.actfact.mtcb.system.SystemType;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class POImpl implements PO
{

	private final Trx trx;
	private final Ctx ctx;
	private final UUID id;
	private boolean isNew;
	private final Type type;
	Map<String, AttributeValue> values = new HashMap<>();

	/**
	 * UUID Constructor for existing instances.
	 * 
	 * @param ctx
	 * @param type
	 * @param id
	 * @param trx
	 */
	public POImpl(Ctx ctx, Type type, UUID id, Trx trx)
	{
		long started = System.nanoTime();
		this.ctx = ctx;
		this.trx = trx;
		this.id = id;
		this.type = type;
		this.isNew = false;
		boolean includeDepencies = type == SystemType.TYPE || type == SystemType.ATTRIBUTE;
		String sql = "SELECT * FROM " + getTableName(ctx, type, includeDepencies) + " t" +
				" WHERE t.id = ? ";
		try (PreparedStatement pstmt = DB.prepareStatement(trx, sql))
		{
			pstmt.setObject(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (!rs.next())
			{
				System.out.println(sql + "[" + id + "]"); 
				throw new SQLException("Not found! Tenant=" + ctx.tenant.getID() + ", ID=" + id);
			}
			for (Attribute attr : type.getAttributes())
			{
				Object value = rs.getObject(attr.getName());
				AttributeValue av = new AttributeValue(attr, value);
				values.put(attr.getName(), av);
			}
			long elapsed = System.nanoTime() - started;
			Timing.register("GET PO by ID", elapsed);
		}
		catch (SQLException e)
		{
			System.err.println(sql + "[" + id + "]");
			throw new UncheckedSQLException(e);
		}
	}

	/**
	 * ResultSet Constructor for existing instances.
	 * 
	 * @param ctx
	 * @param type
	 * @param id
	 * @param trx
	 */
	public POImpl(Ctx ctx, Type type, ResultSet rs, Trx trx) throws SQLException
	{
		this.ctx = ctx;
		this.trx = trx;
		this.type = type;
		this.isNew = false;
		this.id = (UUID) rs.getObject("id");
		for (Attribute attr : type.getAttributes())
		{
			Object value = rs.getObject(attr.getName());
			AttributeValue av = new AttributeValue(attr, value);
			values.put(attr.getName(), av);
		}
	}

	/**
	 * Constructor for new instances
	 * 
	 * @param ctx
	 * @param type
	 * @param trx
	 */
	public POImpl(Ctx ctx, Type type, Trx trx)
	{
		this.ctx = ctx;
		this.trx = trx;
		this.type = type;
		this.id = UUID.randomUUID();
		this.isNew = true;
		for (Attribute attr : type.getAttributes())
		{
			AttributeValue av = new AttributeValue(attr, null);
			values.put(attr.getName(), av);
		}
	}

	@Override
	public UUID getID()
	{
		return id;
	}

	/** Resolve a UUID to a PO object */
	@Override
	public PO idToPO(Attribute attr, UUID id)
	{
		if (SystemPO.contains(id))
			return SystemPO.get(id);

		Type dataType = attr.getDataType();
		switch (dataType.getName())
		{
		case "Type":
			return new TypeImpl(ctx, id, trx);
		case "Attribute":
			return new AttributeImpl(ctx, id, trx);
		case "Tenant":
			return new TenantImpl(ctx, id, trx);
		case "User":
			return new UserImpl(ctx, id, trx);
		default:
			return new POImpl(ctx, dataType, id, trx);
		}
	}

	@Override
	public boolean isNew()
	{
		return isNew;
	}

	@Override
	public Type getType()
	{
		return type;
	}

	public static String getTableName(Ctx ctx, Type type, boolean includeDependencies)
	{
		// Only include dependencies if querying a system type
		if (!includeDependencies) // || type != SystemType.TYPE)
			return ctx.tenant.getName() + "." + type.getName();
		
		String sql = "(SELECT * FROM " +
				ctx.tenant.getName() + "." + type.getName();
		for (Tenant dependency : ctx.dependencies)
			sql += " UNION ALL SELECT * FROM " + dependency.getName() + "." + type.getName();
		return sql + ")";
	}

	@Override
	public Trx getTrx()
	{
		return trx;
	}

	@Override
	public Ctx getContext()
	{
		return ctx;
	}

	@Override
	public boolean save()
	{
		boolean result;
		String sql;
		if (isNew)
		{
			// SQL INSERT
			Object[] params = new Object[values.size() + 2];
			int i = 0;
			params[i++] = getTenant().getID();
			params[i++] = id;
			sql = "Tenant,ID";
			String parameters = "?,?";
			for (AttributeValue av : values.values())
			{
				sql += "," + av.getAttribute().getName();
				parameters += ",?";
				params[i] = av.getValue();
				i++;
			}
			sql = "INSERT INTO " + getTableName(ctx, type, false) + " (" + sql + ") VALUES (" + parameters + ")";
			
			long started = System.nanoTime();
			DB.executeUpdate(trx, sql, params);
			Timing.register("CREATE PO", System.nanoTime() - started);

			isNew = false;
			result = true;
		}
		else
		{
			// SQL UPDATE
			List<Object> params = new ArrayList<>();
			sql = "";
			for (AttributeValue av : values.values())
			{
				if (av.isChanged())
				{
					params.add(av.getValue());
					sql += ", " + av.getAttribute().getName() + " = ?";
				}
			}
			if (sql.length() == 0)
			{
				System.out.println("No values changed");
				result = true;
			}
			else
			{
				sql = "UPDATE " + getTableName(ctx, type, false) + " SET " + sql.substring(1) + " WHERE Tenant_ID = ? AND ID = ?";
				params.add(getTenant().getID());
				params.add(id);
				
				long started = System.nanoTime();
				DB.executeUpdate(trx, sql, params.toArray());
				Timing.register("UPDATE PO", System.nanoTime() - started);
				
				result = true;
			}
		}
		return result;
	}

	@Override
	public void setNew(boolean isNew)
	{
		this.isNew = isNew;
	}

	@Override
	public Map<String, AttributeValue> getValues()
	{
		return values;
	}

}
