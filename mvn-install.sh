#!/bin/sh
cd mtcb-api
mvn clean install
cd ../mtcb-database
mvn clean install
cd ../mtcb-schemabased
mvn clean compile assembly:single
cd ..

