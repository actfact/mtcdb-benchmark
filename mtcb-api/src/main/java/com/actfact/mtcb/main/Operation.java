/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.main;

import java.util.HashMap;
import java.util.Map;

import com.actfact.mtcb.exception.MTCBException;
import com.actfact.mtcb.model.MTCDB;
/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public abstract class Operation
{
	private static final double MARGIN = 0.9;
	
	private String metricName;
	
	/** The min time in ms that a doIt() must take on average. This is to prevent massive creation of
	 * elements that are very fast to create. */
	private long minIterationTime;
	
	private long runningTime;
	
	private int threads;
	
	private MTCDB mtcdb;
	
	private Thread[] ts;
	
	private long n;
	
	private long max;
	
	private int passed = 0;
	
	/** Read-only map with parameters. */
	private Map<String, Object> params = new HashMap<>();
	
	/**
	 * Instantiate an operation. Provide a name for the metric, the number of threads that it should
	 * use, and the minIterationTime, which is the minimum time that each execution of doIt() should take.
	 * <p>
	 * If a minimumIterationTime > 0 is provided, the metric is a boolean: whether it is capable of creating
	 * the according number of instances within the running time. 
	 * <p>
	 * If no minimumIterationTime is provided, the metric is a double: the number of instances created in a minute.
	 * <p>
	 * The resulting metric is printed to the console.
	 * 
	 * @param metricName
	 * @param threads
	 * @param runningTime: running time in seconds
	 * @param minIterationTime
	 */
	public Operation(MTCDB mtcdb, String metricName, int threads, int runningTime, long minIterationTime)
	{
		this.mtcdb = mtcdb;
		this.metricName = metricName;
		this.minIterationTime = minIterationTime;
		this.runningTime = runningTime;
		this.threads = threads;
	}
	
	public int start()
	{
		ts = new Thread[threads];
		for (int i = 0; i < ts.length; i++)
		{
			ts[i] = new Thread()
			{
				@Override
				public void run()
				{
					try (MTCDB newMtcdb = mtcdb.getNewInstance())
					{
						long startTime = System.currentTimeMillis();
						long stopTime = startTime + runningTime * 1000L;
						long max = minIterationTime > 0 ? (stopTime - startTime) / minIterationTime : Long.MAX_VALUE;
						int n = 0;
						while (System.currentTimeMillis() < stopTime)
						{
							doIt(newMtcdb);
							n++;
							if (n == max)
								break;
							if (minIterationTime > 0)
								while (System.currentTimeMillis() < startTime + n * minIterationTime * MARGIN)
									; // Wait when ahead on time
						}
						incN(n);
						incMax(max);
						if (minIterationTime == 0 || n >= max)
							incPassed();
					}
					catch (Exception e)
					{
						throw new MTCBException(e);
					}
				}
			};
		}
		// Start each thread
		for (Thread thread : ts)
			thread.start();
		
		return ts.length;
	}
	
	/**
	 * Call to wait for this operation to finish.
	 * 
	 */
	void report()
	{
		// Wait for all threads to complete
		try
		{
			for (Thread thread : ts)
				thread.join();

			boolean isPassed = passed >= threads; 
			
			System.out.print((isPassed ? "PASSED " : "FAILED ") + metricName + ": ");
			
			if (minIterationTime > 0)
				System.out.println(n + "/" + max);
			else
				System.out.println(1. * n / (runningTime / 60) + " per minute");
		}
		catch (InterruptedException e)
		{
			throw new MTCBException(e);
		}
	}
	
	public Operation setParameter(String name, Object value)
	{
		params.put(name, value);
		return this;
	}
	
	public Object get(String name)
	{
		return params.get(name);
	}
	
	public synchronized void incN(long n)
	{
		this.n += n;
	}
	
	public synchronized void incMax(long max)
	{
		this.max += max;
	}
	
	public synchronized void incPassed()
	{
		this.passed++;
	}

	/** Override with the custom logic for one iteration */
	abstract void doIt(MTCDB mtcdb);
}
