/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.main;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.AttributeValue;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.MTCDB;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.model.User;
import com.actfact.mtcb.system.SystemTenant;
import com.actfact.mtcb.system.SystemType;
import com.actfact.mtcb.system.SystemUser;
import com.actfact.mtcb.util.Util;
/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class Benchmark
{
	public static final String INITIAL_TENANT_DISPLAY_NAME = "InitialTenant";
	
	public static final String SEARCH_TENANT_NAME = "SearchTenant";
	
	public static final String MAIN_MODULE_NAME = "MainModule";
	
	public static final String SEARCH_TYPE_NAME = "CST";
	
	/** The number of attributes for the conjunctive and disjunctive search metrics */
	public static final int SEARCH_ATTRIBUTES = 5;
	
	

	/** Number of Search Type Instances per tenant */
	/**
	 * This method tests if the provided implementation is capable of representing the example multi-tenant
	 * customizable scenario described by Aulbach et al. in their paper "Multi-Tenant databases for software as
	 * a service: schema-mapping techniques".
	 * <p>
	 * If the method finishes without throwing an exception, the data should be successfully imported.
	 * 
	 * @param mtcdb
	 */
	public static void aulbachTest(MTCDB mtcdb)
	{
		try
		{
			mtcdb.initializeSystem();
			// Create the metadata
			Ctx systemCtx = mtcdb.getContext(SystemTenant.SYSTEM, SystemUser.SYSTEM);
			Tenant financeTenant = mtcdb.createTenant(systemCtx, "Finance", "Finance", true);
			Ctx financeCtx = mtcdb.getContext(financeTenant, SystemUser.SYSTEM);
			Type accountType = mtcdb.createType(financeCtx, "Account", "Account");
			mtcdb.createAttribute(financeCtx, accountType, SystemType.STRING, "Name", "Name", true, true);
			Tenant healthCareTenant = mtcdb.createTenant(systemCtx, "HealthCare", "Health Care", true, financeTenant);
			Ctx healthCareCtx = mtcdb.getContext(healthCareTenant, SystemUser.SYSTEM, financeTenant);
			mtcdb.createAttribute(healthCareCtx, accountType, SystemType.STRING, "Hospital", "Hospital", true, true);
			mtcdb.createAttribute(healthCareCtx, accountType, SystemType.NUMBER, "Beds", "Beds", true, true);
			Tenant automotiveTenant = mtcdb.createTenant(systemCtx, "Automotive", "Automotive", true, financeTenant);
			Ctx automotiveCtx = mtcdb.getContext(automotiveTenant, SystemUser.SYSTEM, financeTenant);
			mtcdb.createAttribute(automotiveCtx, accountType, SystemType.NUMBER, "Dealers", "Dealer", true, true);
			Tenant hospitalX = mtcdb.createTenant(systemCtx, "HospitalX", "Hospital X", false, financeTenant, healthCareTenant);
			Ctx hospitalCtx = mtcdb.getContext(hospitalX, SystemUser.SYSTEM, financeTenant, healthCareTenant);
			Tenant bankX = mtcdb.createTenant(systemCtx, "BankX", "Bank X", false, financeTenant);
			Ctx bankCtx = mtcdb.getContext(bankX, SystemUser.SYSTEM, financeTenant);
			Tenant garageX = mtcdb.createTenant(systemCtx, "GarageX", "Garage X", false, financeTenant, automotiveTenant);
			Ctx garageCtx = mtcdb.getContext(garageX, SystemUser.SYSTEM, financeTenant, automotiveTenant);
			// Instantiate Hospital Data
			accountType = mtcdb.getType(healthCareCtx, "Account");
			AttributeValue[] avs = new AttributeValue[3];
			avs[0] = new AttributeValue(accountType.getAttribute("Name"), "Acme");
			avs[1] = new AttributeValue(accountType.getAttribute("Hospital"), "St. Mary");
			avs[2] = new AttributeValue(accountType.getAttribute("Beds"), 135);
			mtcdb.createPO(hospitalCtx, accountType, avs);
			avs[0] = new AttributeValue(accountType.getAttribute("Name"), "Gump");
			avs[1] = new AttributeValue(accountType.getAttribute("Hospital"), "State");
			avs[2] = new AttributeValue(accountType.getAttribute("Beds"), 1042);
			mtcdb.createPO(hospitalCtx, accountType, avs);
			// Instantiate Garage data
			accountType = mtcdb.getType(garageCtx, "Account");
			avs = new AttributeValue[2];
			avs[0] = new AttributeValue(accountType.getAttribute("Name"), "Big");
			avs[1] = new AttributeValue(accountType.getAttribute("Dealers"), 65);
			mtcdb.createPO(garageCtx, accountType, avs);
			// Instantiate the Bank data
			accountType = mtcdb.getType(bankCtx, "Account");
			avs = new AttributeValue[1];
			avs[0] = new AttributeValue(accountType.getAttribute("Name"), "Ball");
			mtcdb.createPO(bankCtx, accountType, avs);
			System.out.println("PASSED Aulbach test");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("FAILED Aulback Test");
		}
		Timing.clear();
	}

	public static void setup(Profile profile, MTCDB mtcdb)
	{
		long started = System.currentTimeMillis();
		mtcdb.initializeSystem();
		Ctx systemCtx = new Ctx(SystemTenant.SYSTEM, SystemUser.SYSTEM);
		// Create meta data module
		Tenant mainTenant = mtcdb.createTenant(systemCtx, MAIN_MODULE_NAME, "Main module", true);
		Ctx tmpMainCtx = new Ctx(mainTenant, SystemUser.SYSTEM);
		User mainModuleUser = mtcdb.createUser(tmpMainCtx, "SuperUser", "info@example.com");
		Ctx mainCtx = new Ctx(mainTenant, mainModuleUser);

		// Creating meta data for master data types
		List<Type> masterDataTypes = IntStream.rangeClosed(1, profile.mdt).parallel()
				.mapToObj((i) -> createMasterDataType(mtcdb, mainCtx, "MDT" + i, "MDT " + i))
				.collect(Collectors.toList());
		System.out.println("Created MDTs");

		// Creating meta data for transaction data types
		List<Type> transactionDataTypes = IntStream.rangeClosed(1, profile.tdt)
				.mapToObj(
						(i) -> createTransactionDataType(mtcdb, mainCtx, "TDT" + i, "TDT " + i, getRandomTypes(masterDataTypes, profile.minra, profile.maxra)))
				.collect(Collectors.toList());
		System.out.println("Created TDTs");

		// Create type to benchmark search on
		Type cst = createSearchType(mtcdb, mainCtx);

		// Create tenant modules and users and store the resulting context
		List<Ctx> ctxs = IntStream.rangeClosed(1, profile.dt)
				.mapToObj((i) -> createTenant(systemCtx, mtcdb, "Tenant" + i, INITIAL_TENANT_DISPLAY_NAME, false, mainTenant)).collect(Collectors.toList());
		System.out.println("Created Tenants");

		// Create ST Instances for each tenant to test the small search
		//ctxs.stream().forEach(ctx -> IntStream.rangeClosed(1, profile.sti).forEach(i -> createSTInstance(ctx, profile, mtcdb, cst, profile.sti)));
		//System.out.println("Created ST instances");
		
		// Create a dedicated tenant for testing search
		Ctx searchTenantCtx = createTenant(systemCtx, mtcdb, SEARCH_TENANT_NAME, "Search Tenant", false, mainTenant);  
		ctxs.add(searchTenantCtx);

		// Create ST Instances for the big search tenant
		IntStream.rangeClosed(1, profile.sti).forEach(i -> createSTInstance(searchTenantCtx, profile, mtcdb, cst, profile.sti));
		System.out.println("Created ST instances");

		// Fill each tenant with master data for each master data type. No. of created records: dt * mdt * mdi
		ctxs.stream().forEach(ctx -> masterDataTypes.stream()
				.forEach(mdt -> IntStream.rangeClosed(1, profile.mdi).forEach(i -> createPO(mtcdb, ctx, mdt, "Data" + i, "Data " + i))));
		System.out.println("Created MD instances");

		System.out.println("Total elapsed: " + ((System.currentTimeMillis() - started) / 1000) + "s");

		try (MTCDB newMtcdb = mtcdb.getNewInstance())
		{
			System.out.println("PASSED Size on Disk: " + newMtcdb.getNewInstance().getSizeOnDisk() + " MB");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("FAILED Size on Disk");
		}
		Timing.printReport();
		Timing.clear();
	}

	public static void main(final Profile profile, MTCDB mtcdb)
	{
		// Declare all final objects that are used by the implemented operations below
		/** All initially created tenants by the setup script */
		final List<Tenant> tenants = mtcdb.getTenants().parallelStream()
				.filter(t -> t.getDisplayName().equals(INITIAL_TENANT_DISPLAY_NAME))
				.collect(Collectors.toList());
		/** The names of all newly created tenants */
		final Set<String> newTenantNames = new HashSet<>();
		/** The system context */
		final Ctx systemCtx = mtcdb.getContext(SystemTenant.SYSTEM, SystemUser.SYSTEM);
		/** The main metadata module */
		final Tenant mainModule = mtcdb.getTenant(MAIN_MODULE_NAME);
		/** The main module context */
		final Ctx mainCtx = mtcdb.getContext(mainModule, SystemUser.SYSTEM);
		/** The big search tenant context */
		final Ctx searchTenantCtx = mtcdb.getContext(mtcdb.getTenant(SEARCH_TENANT_NAME), SystemUser.SYSTEM, mainModule);
		/** The names of tenant and types of newly created types */
		final Set<String[]> tenantTypes = new HashSet<>();
		/** A map from UUID to Type for newly created TD Instances */
		final Map<UUID, Type> createdTdts = new ConcurrentHashMap<>();
		/** The type for the search benchmark */
		final Type csType = mtcdb.getType(mainCtx, SEARCH_TYPE_NAME);

		// Create the operatons
		List<Operation> ops = new ArrayList<>();

		// CREATE TENANTS
		ops.add(new Operation(mtcdb, "Create Tenants", profile.cf, profile.ti, 5000)
		{
			@Override
			void doIt(MTCDB mtcdb)
			{
				String name = "RndTenant_" + random(0, Integer.MAX_VALUE - 1);
				createTenant(systemCtx, mtcdb, name, "RndTenant", false, mainModule);
				newTenantNames.add(name);
			}
		});

		// CREATE TYPES
		ops.add(new Operation(mtcdb, "Create Types", profile.cf, profile.ti, 500)
		{
			@Override
			void doIt(MTCDB mtcdb)
			{
				Tenant tenant = tenants.get(random(0, tenants.size() - 1));
				Ctx ctx = mtcdb.getContext(tenant, SystemUser.SYSTEM);
				String name = "RndType_" + random(0, Integer.MAX_VALUE - 1);
				createType(ctx, profile, mtcdb, name, "RandomType");
				tenantTypes.add(new String[]
				{ name, tenant.getName() });
			}
		});

		// CREATE ATTRIBUTES
		ops.add(new Operation(mtcdb, "Create Attributes", profile.cf, profile.ti, 100)
		{
			@Override
			void doIt(MTCDB mtcdb)
			{
				Tenant tenant = tenants.get(random(0, tenants.size() - 1));
				Ctx ctx = mtcdb.getContext(tenant, SystemUser.SYSTEM, mainModule);
				int tdtSeqNo = random(1, profile.tdt);
				Type tdt = mtcdb.getType(ctx, "TDT" + tdtSeqNo);
				mtcdb.createAttribute(ctx, tdt, SystemType.STRING, "RndAttr_" + random(0, Integer.MAX_VALUE - 1), "Random Attr", true, false);
			}
		});

		// Create TDT Instances
		ops.add(new Operation(mtcdb, "Create TDT Instances", profile.cf, profile.ti, 0)
		{
			@Override
			void doIt(MTCDB mtcdb)
			{
				Tenant tenant = tenants.get(random(0, tenants.size() - 1));
				Ctx ctx = mtcdb.getContext(tenant, SystemUser.SYSTEM, mainModule);
				createPO(ctx, profile, mtcdb, profile.tdt, createdTdts);
			}
		});

		// Load by ID
		ops.add(new Operation(mtcdb, "Load by ID", profile.cf, profile.ti, 0)
		{
			@Override
			void doIt(MTCDB mtcdb)
			{
				while (createdTdts.size() == 0)
					; // Wait until we have at least 1 created TDT
				resolveIds(profile, mtcdb, profile.tdt, createdTdts);
			}
		});

		// Search by Conjunction
		ops.add(new Operation(mtcdb, "Search by 5-way conjunction", profile.cf, profile.ti, 0)
		{
			@Override
			void doIt(MTCDB mtcdb)
			{
				//Tenant tenant = tenants.get(random(0, tenants.size() - 1));
				//Ctx ctx = mtcdb.getContext(tenant, SystemUser.SYSTEM, mainModule);
				conjunctiveSearch(searchTenantCtx, mtcdb, csType, profile.sti);
			}

		});

		// Search by Disjunction
		ops.add(new Operation(mtcdb, "Search by 5-way disjunction", profile.cf, profile.ti, 0)
		{
			@Override
			void doIt(MTCDB mtcdb)
			{
				//Tenant tenant = tenants.get(random(0, tenants.size() - 1));
				//Ctx ctx = mtcdb.getContext(tenant, SystemUser.SYSTEM, mainModule);
				disjunctiveSearch(searchTenantCtx, profile, mtcdb, csType, profile.sti);
			}
		});
		
		// Concurrently run all the operations
		int threads = 0;
		for (Operation op : ops)
			threads += op.start();
		System.out.println("Running " + ops.size() + " operations in " + threads + " threads.");
		for (Operation op : ops)
			op.report();

		Timing.printReport();
		Timing.clear();
	}

	private static Type createMasterDataType(MTCDB mtcdb, Ctx ctx, String name, String displayName)
	{
		Type type = mtcdb.createType(ctx, name, displayName);
		mtcdb.createAttribute(ctx, type, SystemType.STRING, "Name", "Name", true, true);
		mtcdb.createAttribute(ctx, type, SystemType.STRING, "DisplayName", "Display Name", true, true);
		return type;
	}

	private static Type createTransactionDataType(MTCDB mtcdb, Ctx ctx, String name, String displayName, Type... references)
	{
		Type type = mtcdb.createType(ctx, name, displayName);
		mtcdb.createAttribute(ctx, type, SystemType.NUMBER, "Amt1", "Amount 1", true, true);
		mtcdb.createAttribute(ctx, type, SystemType.STRING, "Name", "Name", true, true);
		Arrays.asList(references).stream().forEach((ref) -> mtcdb.createAttribute(ctx, type, ref, ref.getName(), ref.getDisplayName(), true, true));
		return type;
	}

	private static Type createSearchType(MTCDB mtcdb, Ctx ctx)
	{
		Type type = mtcdb.createType(ctx, SEARCH_TYPE_NAME, "Search Type");
		// Five boolean attributes for conjunctive search
		for (int n = 0; n < 5; n++)
			mtcdb.createAttribute(ctx, type, SystemType.NUMBER, "c" + n, "conj", true, true);
		// Five text attributes for disjunctive search
		for (int n = 0; n < 5; n++)
			mtcdb.createAttribute(ctx, type, SystemType.NUMBER, "d" + n, "disj", true, true);
		return type;
	}

	private static PO createSTInstance(Ctx ctx, Profile profile, MTCDB mtcdb, Type type, int size)
	{
		List<AttributeValue> avs = new ArrayList<>();
		type.getAttributes().stream().filter(attr -> attr.getDisplayName().equals("conj")).forEach(attr ->
			avs.add(new AttributeValue(attr, getConjunctiveSearchValue(size, SEARCH_ATTRIBUTES))));
		type.getAttributes().stream().filter(attr -> attr.getDisplayName().equals("disj")).forEach(attr -> 
			avs.add(new AttributeValue(attr, getDisjunctiveSearchValue(size, SEARCH_ATTRIBUTES))));
		return mtcdb.createPO(ctx, type, avs.toArray(new AttributeValue[avs.size()]));
	}
	
	/**
	 * This returns a random value to fill the conjunctive attributes of the search type instances with,
	 * in such a way that when testing the conjunctive search, there is about a 36% chance of getting
	 * no result. For the mathematical explanation of this, please read our accompanying paper.
	 * 
	 * 
	 * @param n
	 * @param a
	 * @return
	 */
	private static int getConjunctiveSearchValue(int n, int a)
	{
		long bound = Math.round(Math.pow(n, 1. / a));
		return ThreadLocalRandom.current().nextInt((int) bound);
	}
	
	/**
	 * This returns a random value to fill the disjunctive attributes of the search type instances with,
	 * in such a way that when testing the disjunctive search, there is about a 36% chance of getting
	 * no result. For the mathematical explanation of this, please read our accompanying paper.
	 *   
	 * 
	 * @param n
	 * @param a
	 * @return
	 */
	private static int getDisjunctiveSearchValue(int n, int a)
	{
		return ThreadLocalRandom.current().nextInt(n * a);
	}

	private static Ctx createTenant(Ctx systemCtx, MTCDB mtcdb, String name, String displayName, boolean isModule, Tenant... dependencies)
	{
		Tenant tenant = mtcdb.createTenant(systemCtx, name, displayName, false, dependencies);
		Ctx tenantCtx = mtcdb.getContext(tenant, SystemUser.SYSTEM);
		User user = mtcdb.createUser(tenantCtx, "admin", "admin@example.com");
		Ctx ctx = mtcdb.getContext(tenant, user);
		return ctx;
	}

	private static void createType(Ctx ctx, Profile profile, MTCDB mtcdb, String name, String displayName)
	{
		Type type = mtcdb.createType(ctx, name, displayName);
		mtcdb.createAttribute(ctx, type, SystemType.STRING, "Name", "Name", true, true);
		mtcdb.createAttribute(ctx, type, SystemType.NUMBER, "Amt", "Amt", true, true);
		mtcdb.createAttribute(ctx, type, SystemType.TIMESTAMP, "Time", "Time", true, true);
	}

	private static void createPO(Ctx ctx, Profile profile, MTCDB mtcdb, int tdtCount, Map<UUID, Type> createdTdts)
	{
		int tdtSeqNo = random(1, tdtCount);
		Type tdt = mtcdb.getType(ctx, "TDT" + tdtSeqNo);
		PO tdtInstance = createPO(ctx, profile, mtcdb, tdt);
		createdTdts.put(tdtInstance.getID(), tdt);
	}

	private static PO createPO(Ctx ctx, Profile profile, MTCDB mtcdb, Type type)
	{
		List<Attribute> attrs = type.getAttributes();
		AttributeValue[] values = new AttributeValue[attrs.size()];
		for (int n = 0; n < attrs.size(); n++)
		{
			Attribute attr = attrs.get(n);
			values[n] = new AttributeValue(attr, getDefaultValue(ctx, profile, mtcdb, attr.getDataType()));
		}
		return mtcdb.createPO(ctx, type, values);
	}

	private static PO createPO(MTCDB mtcdb, Ctx ctx, Type type, String name, String displayName)
	{
		AttributeValue[] avs = new AttributeValue[2];
		avs[0] = new AttributeValue(type.getAttribute("Name"), name);
		avs[1] = new AttributeValue(type.getAttribute("DisplayName"), displayName);
		return mtcdb.createPO(ctx, type, avs);
	}

	private static void resolveIds(Profile profile, MTCDB mtcdb, int tdtCount, Map<UUID, Type> tdtIds)
	{
		// Load a 'random' TDT by id
		for (UUID id : tdtIds.keySet())
		{
			Type type = tdtIds.get(id);
			PO po = mtcdb.getPOByID(type.getContext(), type, id);
			// Resolve all its references
			po.getValues().values().stream().filter(av -> av.getValue() instanceof UUID)
					.forEach(av -> av.setValue(po.idToPO(av.getAttribute(), (UUID) av.getValue())));
			break;
		}
	}

	private static PO conjunctiveSearch(Ctx ctx, MTCDB mtcdb, Type type, int size)
	{
		List<AttributeValue> avs = type.getAttributes().stream().filter(attr -> attr.getDisplayName().equals("conj"))
				.map(attr -> new AttributeValue(attr, getConjunctiveSearchValue(size, SEARCH_ATTRIBUTES))).collect(Collectors.toList());

		List<PO> pos = mtcdb.getByConjunction(ctx, type, avs.toArray(new AttributeValue[avs.size()]), null, 1);
		if (pos.size() > 0)
		{
			return pos.get(0);
		}
		return null;
	}

	private static PO disjunctiveSearch(Ctx ctx, Profile profile, MTCDB mtcdb, Type type, int size)
	{
		List<AttributeValue> avs = type.getAttributes().stream().filter(attr -> attr.getDisplayName().equals("disj"))
				.map(attr -> new AttributeValue(attr, getDisjunctiveSearchValue(size, SEARCH_ATTRIBUTES))).collect(Collectors.toList());

		List<PO> pos = mtcdb.getByDisjunction(ctx, type, avs.toArray(new AttributeValue[avs.size()]), null, 1);
		if (pos.size() > 0)
		{
			return pos.get(0);
		}
		return null;
	}

	private static Object getDefaultValue(Ctx ctx, Profile profile, MTCDB mtcdb, Type dt)
	{
		if (dt == SystemType.STRING)
			return "Data String";
		if (dt == SystemType.NUMBER)
			return BigDecimal.ONE;
		if (dt == SystemType.TIMESTAMP)
			return new Timestamp(System.currentTimeMillis());
		if (dt == SystemType.BOOLEAN)
			return true;
		Attribute nameAttr = dt.getAttribute("Name");
		return mtcdb.getByConjunction(ctx, dt,
				new AttributeValue[]
				{ new AttributeValue(nameAttr, "Data" + (random(1, profile.mdi))) },
				null, 1).get(0);
	}

	private static Type[] getRandomTypes(List<Type> types, int min, int max)
	{
		int n = ThreadLocalRandom.current().nextInt(max - min) + min;
		Type[] result = new Type[n];
		Util.randomSample(types, n).toArray(result);
		return result;
	}

	public static int random(int x, int y)
	{
		return ThreadLocalRandom.current().nextInt(x, y + 1);
	}

	private static void testForCorrectness(Profile profile, MTCDB mtcdb, Set<String> tenantNames, Set<String[]> tenantTypes)
	{
		Tenant mainModule = mtcdb.getTenant("MainModule");
		Ctx mainCtx = mtcdb.getContext(mainModule, SystemUser.SYSTEM);
		Type mdt = mtcdb.getType(mainCtx, "MDT1");
		tenantNames.stream().forEach(name -> {
			Tenant tenant = mtcdb.getTenant(name);
			Ctx ctx = mtcdb.getContext(tenant, SystemUser.SYSTEM, mainModule);
			createPO(mtcdb, ctx, mdt, "Data_" + random(0, Integer.MAX_VALUE - 1), "Data " + random(0, Integer.MAX_VALUE - 1));
		});
		// If no expection is thrown, the tenant is created successfully		
		System.out.println("Tenant creation is correct");

		tenantTypes.stream().forEach(tenantType -> {
			Tenant tenant = mtcdb.getTenant(tenantType[1]);
			Ctx ctx = mtcdb.getContext(tenant, SystemUser.SYSTEM, mainModule);
			Type type = mtcdb.getType(ctx, tenantType[0]);
			createPO(ctx, profile, mtcdb, type);
		});
		// If no expection is thrown, the type was created successfully		
		System.out.println("Type creation is correct");

		// TODO implement attribute creation checker		
		System.out.println("Attribute creation is correct");

		// TODO implement TDI creation checker		
		System.out.println("TDI creation is correct");
	}
}
