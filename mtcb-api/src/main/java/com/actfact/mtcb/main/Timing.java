/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Utility class to register timings for operations. The timings are grouped by
 * the name string. Reported times must be in nanoseconds.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class Timing implements Comparable<Timing>
{
	private static ConcurrentHashMap<String, Timing> timings = new ConcurrentHashMap<>();

	/**
	 * Register a timing for a certain operation that is identified by its name string.
	 * 
	 * @param name Uniquely identifies this operation and is used for grouping
	 * @param time Elapsed time for this operation in nanoseconds.
	 */
	public static void register(String name, long time)
	{
		if (!timings.containsKey(name))
			timings.putIfAbsent(name, new Timing(name));

		Timing timing = timings.get(name);
		synchronized (timing)
		{
			timing.count++;
			timing.sum += time;
			if (time < timing.min)
				timing.min = time;
			if (time > timing.max)
				timing.max = time;
		}
		
	}

	public static void printReport()
	{
		System.out.println(String.format("%-52s%10s%12s%12s%12s%12s", "NAME", "COUNT", "TOTAL(s)", "AVG(ms)", "MIN(ms)", "MAX(ms)"));
		//long sum = 0;
		List<Timing> timingsList = new ArrayList<>(timings.values());
		Collections.sort(timingsList);
		for (Timing timing : timingsList)
		{
			System.out.println(timing);
			//sum += timing.sum;
		}
		// Don't report total because it is meaningless: there might be double measurements
		//System.out.println(String.format("%-60s%12.3f", "TOTAL", sum / 1000000000.));
	}
	
	public static void clear()
	{
		timings = new ConcurrentHashMap<>();
	}

	private Timing(String name)
	{
		this.name = name;
		min = Integer.MAX_VALUE;
	}

	private String name;
	private long sum;
	private int count;
	private long min;
	private long max;

	public String toString()
	{
		return String.format("%-50s%10d%12.3f%12.3f%12.3f%12.3f", name, count, sum / 1000000000., sum / count / 1000000., min / 1000000., max / 1000000.);
	}

	@Override
	public int compareTo(Timing o)
	{
		return name.compareTo(o.name);
	}
}
