/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.main;

/**
 * The benchmark profile. It contains all the parameters needed to run
 * the benchmark.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public enum Profile
{
	TINY(10, 1, 20, 80, 2, 10000, 60, 2, 15), 
	SMALL(100, 5, 100, 400, 2, 100000, 300, 2, 15),
	MEDIUM(1000, 10, 100, 400, 2, 1000000, 300, 2, 15),
	;
	
	/** Number of Data Tenants */
	public final int dt;
	/** Concurrency Factor. The number of threads each Benchmark operation will use. */
	public final int cf;
	/** Number of Master Data Types */
	public final int mdt;
	/** Number of Transaction Data Types */
	public final int tdt;
	/** Number of Master Data Type instances per tenant per type */
	public final int mdi;
	/** Number of Search Type instances */
	public final int sti;
	/** Test interval in seconds */
	public final int ti;
	/** Minimum number of reference attributes on Transaction Type */
	public final int minra;
	/** Maximum number of reference attributes on Transaction Type */
	public final int maxra;
	
	private Profile(int dt, int cf, int mdt, int tdt, int mdi, int sti, int ti, int minra, int maxra)
	{
		this.dt = dt;
		this.cf = cf;
		this.mdt = mdt;
		this.tdt = tdt;
		this.mdi = mdi;
		this.sti = sti;
		this.ti = ti;
		this.minra = minra;
		this.maxra = maxra;
	}
	
	@Override
	public String toString()
	{
		return name() + " [DT=" + dt + ", CF=" + cf + ", MDT=" + mdt + ", TDT=" + tdt + ", MDI=" + mdi + ", STI=" + sti + ", TI=" + ti + ", MINRA=" + minra + ", MAXRA=" + maxra + "]";
	}
}
