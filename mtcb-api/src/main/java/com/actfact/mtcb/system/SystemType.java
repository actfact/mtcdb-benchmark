/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.system;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.actfact.mtcb.exception.NoSuchAttributeException;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.AttributeValue;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.model.User;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public enum SystemType implements Type
{
	STRING(1,"String"),
	NUMBER(2,"Number"),
	BOOLEAN(3,"Boolean"),
	TIMESTAMP(4,"Timestamp"),
	TYPE(5,"Type",SystemAttribute.TYPE_NAME, SystemAttribute.TYPE_DISPLAYNAME),
	ATTRIBUTE(6,"Attribute",SystemAttribute.ATTRIBUTE_NAME, SystemAttribute.ATTRIBUTE_DISPLAYNAME, SystemAttribute.ATTRIBUTE_MASTERTYPE, SystemAttribute.ATTRIBUTE_DATATYPE, SystemAttribute.ATTRIBUTE_ISINDEXED, SystemAttribute.ATTRIBUTE_ISMANDATORY, SystemAttribute.ATTRIBUTE_COLNO),
	TENANT(7,"Tenant",SystemAttribute.TENANT_NAME, SystemAttribute.TENANT_DISPLAYNAME,SystemAttribute.TENANT_ISMODULE),
	USER(8,"User",SystemAttribute.USER_NAME, SystemAttribute.USER_EMAIL),
	COMPLEX(9,"Complex") // The complex type is just a placeholder type to recognize complex data types
	;
	
	private final UUID id;
	private final Timestamp created;
	private final String name;
	private final List<Attribute> attributes;
	private final Ctx ctx;
	
	private SystemType(long id, String name, Attribute... attributes)
	{
		this.id = new UUID(3,id);
		this.name = name;
		this.created = new Timestamp(0);
		this.attributes = new ArrayList<>();
		this.ctx = new Ctx(SystemTenant.SYSTEM, SystemUser.SYSTEM);
		for (Attribute attribute : attributes)
			this.attributes.add(attribute);
	}

	@Override
	public UUID getID() {
		return id;
	}

	@Override
	public Tenant getTenant() {
		return SystemTenant.SYSTEM;
	}

	@Override
	public Timestamp getCreated() {
		return created;
	}

	@Override
	public User getCreatedBy() {
		return SystemUser.SYSTEM;
	}

	@Override
	public Timestamp getUpdated() {
		return created;
	}

	@Override
	public User getUpdatedBy() {
		return SystemUser.SYSTEM;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public String getValueAsString(String attributeName) {
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public <T extends PO> T getValueAsPO(String attributeName) {
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public BigDecimal getValueAsNumber(String attributeName) {
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public Boolean getValueAsBoolean(String attributeName) {
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public Timestamp getValueAsTimestamp(String attributeName) {
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public boolean isNew() {
		return false;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public List<Attribute> getAttributes() {
		return attributes;
	}

	@Override
	public Attribute getAttribute(String name) {
		throw new NoSuchAttributeException(name, name);
	}

	@Override
	public Type getType() {
		return this;
	}

	@Override
	public boolean save()
	{
		return true;
	}

	@Override
	public Ctx getContext()
	{
		return ctx;
	}

	@Override
	public Trx getTrx()
	{
		return null;
	}

	@Override
	public void setNew(boolean isNew)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setActive(boolean isActive)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setString(String attributeName, String string)
	{
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public <T extends PO> void setPO(String attributeName, T t)
	{
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public void setBoolean(String attributeName, Boolean bool)
	{
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public void setTimestamp(String attributeName, Timestamp timestamp)
	{
		throw new NoSuchAttributeException(name, attributeName);
	}

	@Override
	public void setName(String name)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDisplayName(String displayName)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void addAttributes(Attribute... attr)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObject(String attributeName, Object object)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PO idToPO(Attribute attr, UUID id)
	{
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Map<String, AttributeValue> getValues()
	{
		return null;
	}

	@Override
	public Attribute getAttribute(UUID id)
	{
		throw new NoSuchAttributeException(name, id.toString());
	}

}
