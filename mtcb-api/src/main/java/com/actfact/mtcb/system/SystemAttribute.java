/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.system;

import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

import com.actfact.mtcb.exception.NoSuchAttributeException;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.AttributeValue;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.model.User;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public enum SystemAttribute implements Attribute
{
	TYPE_NAME(1,"Name","Name",SystemType.TYPE, SystemType.STRING, true, true),
	TYPE_DISPLAYNAME(2,"DisplayName","Display Name",SystemType.TYPE, SystemType.STRING, false, true),
	ATTRIBUTE_NAME(11,"Name","Name", SystemType.ATTRIBUTE, SystemType.STRING, true, true),
	ATTRIBUTE_DISPLAYNAME(12,"DisplayName","Display Name", SystemType.ATTRIBUTE, SystemType.STRING, false, true),
	ATTRIBUTE_MASTERTYPE(13,"MasterType","Master Type", SystemType.ATTRIBUTE, SystemType.TYPE, false, true)
	{
		// Solution with polymorphism is needed to avoid null values. The problem is that the enum constructors refer to each other, so
		// the one that gets loaded first will have nulls for references to the one that gets loaded later.
		@Override
		public Type getDataType()
		{
			return SystemType.TYPE;
		}
	},
	ATTRIBUTE_DATATYPE(14,"DataType","Data Type", SystemType.ATTRIBUTE, SystemType.TYPE, false, true)
	{
		@Override
		public Type getDataType()
		{
			return SystemType.TYPE;
		}
	},
	ATTRIBUTE_ISINDEXED(15,"IsIndexed","Indexed", SystemType.ATTRIBUTE, SystemType.BOOLEAN, false, true),
	ATTRIBUTE_ISMANDATORY(16,"IsMandatory","Mandatory", SystemType.ATTRIBUTE, SystemType.BOOLEAN, false, true),
	ATTRIBUTE_COLNO(17, "ColNo", "Column Number", SystemType.ATTRIBUTE, SystemType.NUMBER, false, false),
	TENANT_NAME(21,"Name","Name",SystemType.TENANT, SystemType.STRING, true, false),
	TENANT_DISPLAYNAME(22,"DisplayName","Display Name",SystemType.TENANT, SystemType.STRING, true, false),
	TENANT_ISMODULE(23,"IsModule", "Module", SystemType.TENANT, SystemType.BOOLEAN, false, true),
	USER_NAME(31,"Name","Name",SystemType.USER, SystemType.STRING, true, false),
	USER_EMAIL(32,"Email","Email",SystemType.USER, SystemType.STRING, false, false),
	GLOBAL_CREATED(41,"Created","Created", null, SystemType.TIMESTAMP, false, false),
	;
	
	/*
	static
	{
		System.out.println("Initialized System Attributes");
		for (SystemAttribute a : values())
			System.out.println(a.toString());
	}*/
	
	private final UUID id;
	private final Timestamp created;
	private final String name;
	private final String displayName;
	private final Ctx ctx;
	private final Type masterType;
	private final Type dataType;
	private final boolean isIndexed;
	private final boolean isMandatory;
	
	private SystemAttribute(long id, String name, String displayName, Type masterType, Type dataType, boolean isIndexed, boolean isMandatory)
	{
		this.id = new UUID(4,id);
		this.name = name;
		this.displayName = displayName;
		this.created = new Timestamp(0);
		this.ctx = new Ctx(SystemTenant.SYSTEM, SystemUser.SYSTEM);
		this.masterType = masterType;
		this.dataType = dataType;
		this.isIndexed = isIndexed;
		this.isMandatory = isMandatory;
	}

	@Override
	public UUID getID() {
		return id;
	}

	@Override
	public Tenant getTenant() {
		return SystemTenant.SYSTEM;
	}

	@Override
	public Timestamp getCreated() {
		return created;
	}

	@Override
	public User getCreatedBy() {
		return SystemUser.SYSTEM;
	}

	@Override
	public Timestamp getUpdated() {
		return created;
	}

	@Override
	public User getUpdatedBy() {
		return SystemUser.SYSTEM;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public boolean isNew() {
		return false;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}

	@Override
	public Type getType() {
		return SystemType.ATTRIBUTE;
	}

	@Override
	public boolean save()
	{
		return true;
	}

	@Override
	public Ctx getContext()
	{
		return ctx;
	}

	@Override
	public Trx getTrx()
	{
		return null;
	}

	@Override
	public void setNew(boolean isNew)
	{
	}

	@Override
	public void setActive(boolean isActive)
	{
		
	}

	@Override
	public void setString(String attributeName, String string)
	{
		throw new NoSuchAttributeException("Attribute", attributeName);
	}

	@Override
	public <T extends PO> void setPO(String attributeName, T t)
	{
		throw new NoSuchAttributeException("Attribute", attributeName);
	}

	@Override
	public void setBoolean(String attributeName, Boolean bool)
	{
		throw new NoSuchAttributeException("Attribute", attributeName);
	}

	@Override
	public void setTimestamp(String attributeName, Timestamp timestamp)
	{
		throw new NoSuchAttributeException("Attribute", attributeName);
	}

	@Override
	public Type getMasterType()
	{
		return masterType;
	}

	@Override
	public Type getDataType()
	{
		return dataType;
	}

	@Override
	public boolean isIndexed()
	{
		return isIndexed;
	}

	@Override
	public boolean isMandatory()
	{
		return isMandatory;
	}
	
	@Override
	public String toString()
	{
		return "[Name=" + getName() + ",DataType=" + getDataType() + ",MasterType=" + getMasterType() + "]";
	}

	@Override
	public void setName(String name)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDisplayName(String displayName)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setMasterType(Type type)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDataType(Type type)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setIndexed(boolean isIndexed)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setMandatory(boolean isMandatory)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObject(String attributeName, Object object)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PO idToPO(Attribute attr, UUID id)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<String, AttributeValue> getValues()
	{
		return null;
	}

	@Override
	public SystemType getBaseType()
	{
		return (SystemType) dataType;
	}
}
