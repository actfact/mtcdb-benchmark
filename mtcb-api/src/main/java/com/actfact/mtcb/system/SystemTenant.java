/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.system;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.actfact.mtcb.exception.NoSuchAttributeException;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.AttributeValue;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.model.User;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public enum SystemTenant implements Tenant {
	SYSTEM;

	private final UUID id;
	private final Timestamp created;
	private final String name;
	private final String displayName;
	private final Ctx ctx;

	private SystemTenant() {
		this.id = new UUID(1, 1);
		this.created = new Timestamp(0);
		this.name = "System";
		this.displayName = "System";
		this.ctx = new Ctx(this, SystemUser.SYSTEM);
	}

	@Override
	public UUID getID() {
		return id;
	}

	@Override
	public Tenant getTenant() {
		return this;
	}

	@Override
	public Timestamp getCreated() {
		return created;
	}

	@Override
	public User getCreatedBy() {
		return SystemUser.SYSTEM;
	}

	@Override
	public Timestamp getUpdated() {
		return created;
	}

	@Override
	public User getUpdatedBy() {
		return SystemUser.SYSTEM;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public String getValueAsString(String attributeName) {
		throw new NoSuchAttributeException("Tenant", attributeName);
	}

	@Override
	public <T extends PO> T getValueAsPO(String attributeName) {
		throw new NoSuchAttributeException("Tenant", attributeName);
	}

	@Override
	public BigDecimal getValueAsNumber(String attributeName) {
		throw new NoSuchAttributeException("Tenant", attributeName);
	}

	@Override
	public Boolean getValueAsBoolean(String attributeName) {
		throw new NoSuchAttributeException("Tenant", attributeName);
	}

	@Override
	public Timestamp getValueAsTimestamp(String attributeName) {
		throw new NoSuchAttributeException("Tenant", attributeName);
	}

	@Override
	public boolean isNew() {
		return false;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}

	@Override
	public Type getType() {
		return SystemType.TENANT;
	}

	@Override
	public boolean save()
	{
		return true;
	}

	@Override
	public Ctx getContext()
	{
		return ctx;
	}

	@Override
	public Trx getTrx()
	{
		return null;
	}

	@Override
	public void setNew(boolean isNew)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setActive(boolean isActive)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setString(String attributeName, String string)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public <T extends PO> void setPO(String attributeName, T t)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBoolean(String attributeName, Boolean bool)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setTimestamp(String attributeName, Timestamp timestamp)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setName(String name)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDisplayName(String displayName)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObject(String attributeName, Object object)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isModule()
	{
		return true;
	}

	@Override
	public void setModule(boolean isModule)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PO idToPO(Attribute attr, UUID id)
	{
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Map<String, AttributeValue> getValues()
	{
		return null;
	}
}
