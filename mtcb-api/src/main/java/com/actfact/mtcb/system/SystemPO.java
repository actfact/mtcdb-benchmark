/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.system;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.actfact.mtcb.model.PO;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class SystemPO
{
	private static final Map<UUID, PO> systemPOs = new HashMap<>();

	static
	{
		PO[][] sysPos = new PO[][]{ SystemAttribute.values(), SystemTenant.values(), SystemType.values(), SystemUser.values() };
		for (PO[] pos : sysPos)
			for (PO po : pos)
				if (systemPOs.containsKey(po.getID()))
					throw new IllegalStateException("Duplicate System type ID: " + po.getID());
				else
					systemPOs.put(po.getID(), po);
	}

	/** Retrieve system PO object by UUID. Returns null if there is no system PO object for that id. */
	public static PO get(UUID id)
	{
		return systemPOs.get(id);
	}
	
	public static boolean contains(UUID id)
	{
		return systemPOs.containsKey(id);
	}
}
