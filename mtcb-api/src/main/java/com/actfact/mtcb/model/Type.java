/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

import java.util.List;
import java.util.UUID;

/**
 * Type is the main meta-data building block of the system.
 * A simple type is a type with no Attributes.
 * A complex type has one or more attributes.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public interface Type extends PO
{
	/**
	 * A textual identifier for this Type. Must be unique within a tenant.
	 * 
	 */
	default String getName()
	{
		return getValueAsString("Name");
	}
	
	/**
	 * A name used for display to the user.
	 * 
	 */
	default String getDisplayName()
	{
		return getValueAsString("DisplayName");
	}

	/**
	 * Get all the attributes for this type in the current context.
	 * 
	 */
	List<Attribute> getAttributes();
	
	/**
	 * Get a single attribute for this type in the current context by name.
	 * 
	 */
	Attribute getAttribute(String name);
	
	/**
	 * Get a single attribute for this type in the current context by id.
	 * 
	 */
	Attribute getAttribute(UUID id);
	
	/**
	 * Set the name for this type.
	 * 
	 */
	default void setName(String name)
	{
		setString("Name", name);
	}
	
	/**
	 * Set the display name for this type.
	 * 
	 */
	default void setDisplayName(String displayName)
	{
		setString("DisplayName", displayName);
	}
	
	/**
	 * Add one or more attributes to this type.
	 * 
	 */
	void addAttributes(Attribute... attr);
	
}
