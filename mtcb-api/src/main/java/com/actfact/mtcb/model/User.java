/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public interface User extends PO
{
	/**
	 * An identifier for the user as an e-mail address as specified by
	 * RFC822: http://www.freesoft.org/CIE/RFC/822/.
	 * 
	 * Must be unique within a tenant.
	 * 
	 */
	default String getEmail()
	{
		return getValueAsString("Email");
	}
	
	/**
	 * Display name for this user.
	 * 
	 */
	default String getName()
	{
		return getValueAsString("Name");
	}
	
	/**
	 * Set the email. Must throw an exception if email is not a valid email
	 * address according to RFC822.
	 * 
	 */
	default void setEmail(String email)
	{
		setString("Email", email);
	}
	
	/**
	 * Set the name.
	 * 
	 */
	default void setName(String name)
	{
		setString("Name", name);
	}
}
