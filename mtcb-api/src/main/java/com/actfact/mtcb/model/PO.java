/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Persistent Object. An object that must be persisted in
 * permanent storage.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 *
 */
public interface PO
{
	
	
	/** 128-bit universally unique identifier for this instance
	 * as defined in RFC4122: https://tools.ietf.org/html/rfc4122.
	 * 
	 * Because of the size of the identifier, uniqueness can virtually ensured
	 * by randomization, and no central registration mechanism is needed.
	 * 
	 */
	UUID getID();
	
	Map<String, AttributeValue> getValues();
	
	/** 
	 * The Tenant owner of this record.
	 */
	default Tenant getTenant()
	{
		return getContext().tenant;
	}
	
	/** When this instance was created, in UTC time and at least millisecond precision. */
	default Timestamp getCreated()
	{
		return getValueAsTimestamp("Created");
	}
	
	/** The user that created this instance. */
	default User getCreatedBy()
	{
		return getValueAsPO("CreatedBy");
	}
	
	/** When this instance was last updated, in UTC time and at least millisecond precision.  */
	default Timestamp getUpdated()
	{
		return getValueAsTimestamp("Updated");
	}
	
	/** The user that last updated this instance. */
	default User getUpdatedBy()
	{
		return getValueAsPO("UpdatedBy");
	}
	
	/** Whether this instance is active. An inactive instance
	 * merely exists for historical purposes. */
	default boolean isActive()
	{
		return getValueAsBoolean("IsActive");
	}
	
	/**
	 * Retrieve the value for the attribute with this name as a String. Must throw an
	 * exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default String getValueAsString(String attributeName)
	{
		return (String) getValue(attributeName);
	}
	
	default Object getValue(String attributeName)
	{
		return getValues().get(attributeName).getValue();
	}
	
	/**
	 * Retrieve the value for the attribute with this name as a PO or child of PO. Must throw an
	 * exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default <T extends PO> T getValueAsPO(String attributeName)
	{
		AttributeValue av = getValues().get(attributeName);
		if (av.getValue() instanceof UUID)
		{
			// Lazy loading!
			av.setValue(idToPO(av.getAttribute(), (UUID) av.getValue()));
		}
		return (T) av.getValue();
	}
	
	PO idToPO(Attribute attr, UUID id);

	/**
	 * Retrieve the value for the attribute with this name as a Number. Must throw an
	 * exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default BigDecimal getValueAsNumber(String attributeName)
	{
		return (BigDecimal) getValue(attributeName);
	}
	
	/**
	 * Retrieve the value for the attribute with this name as a Three-valued Boolean (null, true or false).
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 */	
	default Boolean getValueAsBoolean(String attributeName)
	{
		return (Boolean) getValue(attributeName);
	}
	
	/**
	 * Retrieve the value for the attribute with this name as a Timestamp in UTC and at millisecond precision.
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default Timestamp getValueAsTimestamp(String attributeName)
	{
		return (Timestamp) getValue(attributeName);
	}
	
	/** Whether this instance is new, i.e. not yet saved in persistent storage
	 */
	boolean isNew();
	
	/**
	 * The type of this PO.
	 */
	Type getType();

	/**
	 * Set this PO to be active. Inactive objects only exist for historical purposes.
	 * 
	 * @param isActive
	 */
	default void setActive(boolean isActive)
	{
		setBoolean("IsActive", isActive);
	}

	/**
	 * Set the value for the attribute with this name.
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default void setString(String attributeName, String string)
	{
		setObject(attributeName, string);
	}
	
	/**
	 * Set the value for the attribute with this name.
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 * @param attributeName
	 * @param value
	 */
	default void setNumber(String attributeName, BigDecimal value)
	{
		setObject(attributeName, value);
	}

	/**
	 * Set the value for the attribute with this name.
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default <T extends PO> void setPO(String attributeName, T po)
	{
		setObject(attributeName, po);
	}
	
	/**
	 * Set the value for the attribute with this name.
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default void setBoolean(String attributeName, Boolean bool)
	{
		setObject(attributeName, bool);
	}

	/**
	 * Set the value for the attribute with this name.
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default void setTimestamp(String attributeName, Timestamp timestamp)
	{
		setObject(attributeName, timestamp);
	}
	/**
	 * Set the value for the attribute with this name.
	 * Must throw an exception if no attribute of that name and type exists for this PO.
	 * 
	 */
	default void setObject(String attributeName, Object object)
	{
		getValues().get(attributeName).setValue(object);
	}
	
	/** To persist this instance. Returns true if changes were persisted, false otherwise */
	boolean save();

	/**
	 * Get the context for this PO. The context contains at least the tenant and the user.
	 * 
	 */
	Ctx getContext();
	
	/**
	 * Get the MTC-DB transaction for this PO. This is the transaction that was used to create it
	 * or that was used to retrieve it. 
	 */
	Trx getTrx();
	
	/**
	 * Set the IsNew status of this PO.
	 * 
	 */
	void setNew(boolean isNew);
}
