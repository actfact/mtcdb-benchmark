/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

/**
 * Specifies context in which a call is made. All calls to the api require this
 * object.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 *
 */
public class Ctx
{
	public final Tenant tenant;
	public final User user;
	public final Tenant[] dependencies;
	
	public Ctx(Tenant tenant, User user, Tenant...dependencies)
	{
		this.tenant = tenant;
		this.user = user;
		this.dependencies = dependencies;
	}

}
