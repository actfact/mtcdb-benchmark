/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

import java.util.List;
import java.util.UUID;

/**
 * The public interface for the Data Storage implementation.
 * 
 * Each method on this interface must be implemented to be ACID compliant:
 * 
 * Atomic, Consistent, Isolated and Durable.
 * 
 * If a method successfully completes, all changes should persist.
 * 
 * If the method terminates with an exception, the MTC-DB should be in the same state
 * as it was before calling the method.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 *
 */
public interface MTCDB extends AutoCloseable
{
	/** Set up the most elemental metadata necessary for the system.
	 * After calling this method the system has created all basic meta-data:
	 * A System Tenant, a System User and the System Types: String, Number, Boolean, Timestamp,
	 * Tenant, User, Type and Attribute. 
	 */
	void initializeSystem();
	
	/**
	 * Retrieves the total size on disk in MB of the MTC-DB.
	 * 
	 * @return
	 */
	long getSizeOnDisk();
	
	/**
	 * Create a new empty tenant.
	 * 
	 * @param ctx Context for the transaction: contains at least tenant and user
	 * @param name Unique textual identifier
	 * @param displayName Name for display to the user
	 * @param isModule Whether this is a metadata module
	 * @param dependencies The modules that the new module depends on
	 * @return The created Tenant.
	 */
	Tenant createTenant(Ctx ctx, String name, String displayName, boolean isModule, Tenant... dependencies);
	
	/**
	 * Create a new user for the provided context.
	 * 
	 * @param ctx Context for the transaction: contains at least tenant and user
	 * @param name Full name of the user.
	 * @param email Unique identifier within this tenant.
	 * @return The created User
	 */
	User createUser(Ctx ctx, String name, String email);

	/**
	 * Create a new type without attributes.
	 * 
	 * @param ctx Context for the transaction: contains at least tenant and user
	 * @param name The lookup name for the new attribute
	 * @param displayName The displayed name for the new attribute
	 * @return The created Type.
	 */
	Type createType(Ctx ctx, String name, String displayName);
	
	/**
	 * Create a new instance of an existing type.
	 * 
	 * @param ctx Context for the transaction: contains at least tenant and user
	 * @param type The type of the PO
	 * @param values The values for all attributes of this type
	 * @return The created PO
	 */
	PO createPO(Ctx ctx, Type type, AttributeValue... values);

	/**
	 * Add a new attribute to an existing type
	 * 
	 * @param ctx Context for the transaction: contains at least tenant and user
	 * @param masterType The type this attribute belongs to.
	 * @param dataType The data type for this attribute.
	 * @param name Unique identifier within this type
	 * @param displayName Name for display to the user 
	 * @param isIndexed Whether this attribute is indexed and thus searchable
	 * @param isMandatory Whether this attribute is mandatory for all instances of its master type.
	 * @return The created Attribute.
	 */
	Attribute createAttribute(Ctx ctx, Type masterType, Type dataType, String name, String displayName, boolean isIndexed, boolean isMandatory);
	
	/**
	 * Get a context for this tenant and user.
	 * 
	 * @param tenant The tenant that will own any records created using this context.
	 * @param user The user that will be the creator or updater of any inserts or updates using this context.
	 * @return The context
	 */
	Ctx getContext(Tenant tenant, User user, Tenant... dependencies);
	
	/**
	 * Add a metadata module to the tenant in the context.
	 * After this method was called, the tenant in the context can
	 * use all the types and attributes that are defined in the added module.
	 * 
	 * @param ctx Context for the transaction: contains at least tenant and user
	 * @param module The module for which the metadata is added to the context tenant.
	 */
	void addModule(Ctx ctx, Tenant module);
	
	/**
	 * Retrieve PO instances by conjunction (AND) of attributes and values.
	 * 
	 * @param type The type of the return type. Mandatory.
	 * @param attributes The attributes to match by conjunction. Use null to indicate to select all records.
	 * @param values The values that the respective attributes must match.
	 * @param orderAttributes The attributes to order by, in order of priority
	 * @param limit The maximum number of results to return. Use 0 for no limit.
	 * @return A list of instantiated PO types
	 */
	List<PO> getByConjunction(Ctx ctx, Type type, AttributeValue[] avs, Attribute[] orderAttributes, int limit);
	//<T extends PO, K extends T> List<T> getByConjunction(Class<K> clazz, Ctx ctx, Type type,
	//		Attribute[] attributes, Object[] values, Attribute[] orderAttributes, int limit);
	
	/**
	 * Retrieve PO instances by disjunction (OR) of attributes and values.
	 * 
	 * @param ctx
	 * @param type
	 * @param avs
	 * @param orderAttributes
	 * @param limit
	 * @return
	 */
	List<PO> getByDisjunction(Ctx ctx, Type type, AttributeValue[] avs, Attribute[] orderAttributes, int limit);
	
	/**
	 * Retrieve a PO instance by ID
	 * 
	 * @param ctx
	 * @param type
	 * @param id
	 * @return
	 */
	PO getPOByID(Ctx ctx, Type type, UUID id);
	
	/**
	 * Get all Tenants that exist in the system (the system tenant may be excluded from the result).
	 * 
	 */
	List<Tenant> getTenants();

	/**
	 * Get tenant by name.
	 * 
	 * @param name
	 * @return
	 */
	Tenant getTenant(String name);
	
	/**
	 * Get all Types that exist for the given Tenant.
	 * 
	 */
	List<Type> getTypes(Ctx ctx);
	
	/**
	 * Retrieve a Type from the given Tenant by Name
	 * 
	 */
	Type getType(Ctx ctx, String name);
	
	/** Get a new ACID compliant transaction */
	Trx getTrx(String name);
	
	/** Get a new instance of this MTC-DB. This is useful when emulating parallel operations. */
	MTCDB getNewInstance();
	
	default boolean saveAndCommit(PO po)
	{
		if (po.save())
		{
			po.getTrx().commit();
			return true;
		}
		else
		{
			po.getTrx().rollback();
			return false;
		}
	}
	
}
