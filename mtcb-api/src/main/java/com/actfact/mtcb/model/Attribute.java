/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

import java.math.BigDecimal;

import com.actfact.mtcb.system.SystemType;

/**
 * Attributes define Type. Every attribute belongs to a type (its Master Type) and is
 * of a type itself (its Data Type). Attributes may be indexed, mandatory and have a name
 * and a display name.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public interface Attribute extends PO
{
	/**
	 * A textual identifier for this Attribute. Must be unique within the master type.
	 * 
	 */
	default String getName()
	{
		return getValueAsString("Name");
	}
	
	/**
	 * A name used for display to the user.
	 * 
	 */
	default String getDisplayName()
	{
		return getValueAsString("DisplayName");
	}

	/**
	 * The type this attribute belongs to.
	 * 
	 */
	default Type getMasterType()
	{
		return getValueAsPO("MasterType");
	}
	
	/**
	 * The data type of this attribute.
	 * 
	 */
	default Type getDataType()
	{
		return getValueAsPO("DataType");
	}
	
	/**
	 * Whether this attribute is indexed. If it is indexed, it must be optimized for
	 * search.
	 * 
	 */
	default boolean isIndexed()
	{
		return getValueAsBoolean("IsIndexed");
	}
	
	/**
	 * Whether this attribute is mandatory. If it is mandatory, the MTCDB should not allow
	 * instances of the master type to be created without having a value for this attribute.
	 * 
	 */
	default boolean isMandatory()
	{
		return getValueAsBoolean("IsMandatory");
	}
	
	/**
	 * The sequential number of this attribute. Not mandatory, but may be useful for some implementations.
	 * 
	 * @return
	 */
	default BigDecimal getColNo()
	{
		return this.getValueAsNumber("ColNo");
	}
	
	/**
	 * Set the name.
	 * 
	 */
	default void setName(String name)
	{
		setString("Name", name);
	}
	
	/**
	 * Set the display name.
	 * 
	 */
	default void setDisplayName(String displayName)
	{
		setString("DisplayName", displayName);
	}
	
	/**
	 * Set the master type.
	 * 
	 */
	default void setMasterType(Type type)
	{
		setPO("MasterType", type);
	}
	
	/**
	 * Set the data type.
	 * 
	 */
	default void setDataType(Type type)
	{
		setPO("DataType", type);
	}
	
	/**
	 * Set whether this attribute should be indexed.
	 * 
	 */
	default void setIndexed(boolean isIndexed)
	{
		setBoolean("IsIndexed", isIndexed);
	}
	
	/**
	 * Set whether this attribute should be mandatory.
	 * 
	 */
	default void setMandatory(boolean isMandatory)
	{
		setBoolean("IsMandatory", isMandatory);
	}
	
	/**
	 * Optionally set the column number for this attribute.
	 * 
	 * @param value
	 */
	default void setColNo(BigDecimal value)
	{
		setObject("ColNo", value);
	}
	
	/**
	 * Get the BaseType of this attribute, i.e.,
	 * String, Number, Boolean, Timestamp or Complex.
	 * 
	 * @return
	 */
	default SystemType getBaseType()
	{
		return null; // TODO evaluate if this is method necessary (added for RowBased)
	}
	
}
